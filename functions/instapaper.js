exports.handler = async (event) => {
  if (event.httpMethod === "POST") {
    console.log('POST req event body:');
    console.log(event.body);
    return { statusCode: 200, body: "POST req submitted" };
  }

  // TODO implement
  const response = {
    statusCode: 200,
    body: JSON.stringify('Hello from Lambda!'),
  };
  return response;
};
