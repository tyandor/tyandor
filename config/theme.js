const colors = {
  primary: '#66FCF1',
  primaryDark: '#45A29E',
  bg: '#C4C5C6',
  black: '#0B0C10',
  dark: '#1F2833',
  light: '#C4C5C6',
  gray: {
    aaa: '#AAAAAA',
    bbb: '#BBBBBB',
    ccc: '#CCCCCC',
    ddd: '#DDDDDD',
    whitesmoke: '#F5F5F5',
    dark: '#474747'
  },
  white: 'white',
  red: '#C9433C',
}

const fontSize = {
  small: '0.7rem',
  large: '1.3rem',
}

const fontFamily = {
  serif: `'Spectral', -apple-system, 'Georgia', 'Times New Roman', serif`,
  sans: `'Overpass', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Helvetica', 'Arial', sans-serif`,
  mono: `'Fira Code', monospace`,
}

const breakpoints = {
  tablet: '1200px',
  phone: '600px',
}

const transitions = {
  normal: '0.5s',
}

const theme = {
  colors,
  fontSize,
  breakpoints,
  fontFamily,
  transitions,
  maxWidth: '1200px',
  baseFontSize: '22px',
}

export default theme
