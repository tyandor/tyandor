# tyandor.com

---

[![Netlify Status](https://api.netlify.com/api/v1/badges/d5f533cb-61d8-4157-aeb3-c5d2741afb75/deploy-status)](https://app.netlify.com/sites/tyandor-new/deploys)

## Description

Personal website for Tyler Andor at [tyandor.com](https://tyandor.com)

---

### Architecture

- React
- Gatsby
- MDX: Documentation [here](https://github.com/mdx-js/mdx) and [here](https://github.com/ChristopherBiscardi/gatsby-mdx)
- Styled-Components

---

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
