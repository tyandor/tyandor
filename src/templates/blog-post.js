import React from 'react'
import { Link, graphql } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import { MDXProvider } from "@mdx-js/react"
import styled from 'styled-components'
import theme from '../../config/theme'
import Bio from '../components/Bio'
import Layout from '../components/Layout'
import Content from '../components/Content'
import SEO from '../components/seo'
import Goodreads from '../components/Goodreads'
import '../utils/styles/components/twitter-bio.scss'
import Mountains from '../components/Mountains'

const shortcodes = {
  Goodreads,
}

const PrevNext = styled.div`
  width: 95%;
  margin: 4rem auto -9rem auto;
  padding-bottom: 12rem;

  ul {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    list-style-type: none;
    margin: 0 auto;
    padding: 1rem 0;

    li {
      margin-bottom: 0;

      a {
        padding: 0.5rem 1rem;
        border: 1px solid ${props => props.theme.colors.primaryDark};
        border-radius: 3px;
      }

      &::before, &::after { display: none }
    }

    @media (min-width: ${props => props.theme.breakpoints.phone}) {
      justify-content: space-between;
    }
  }

  @media (min-width: ${props => props.theme.breakpoints.phone}) {
    width: 89%;
    justify-content: space-between;
  }
  @media (min-width: ${props => props.theme.breakpoints.tablet}) {
    width: 75%;
  }
`

const PostFooter = styled.footer`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 3.3rem auto -1rem;
  font-family: ${props => props.theme.fontFamily.sans};
  font-size: ${props => props.theme.fontSize.small};
  max-width: 960px;
`

const Title = styled.header`
  margin: 0 auto;
  align-items: center;
  display: flex;
  height: auto;
  justify-content: center;
  max-width: 90%;

  h1 {
    color: ${props => props.theme.colors.white};
    font-size: 120px;
    text-align: center;
    margin: 7rem auto 1rem;
  }
`

const Discovery = styled.div`
  margin: 0 auto;
  width: 90%;
`

class BlogPostTemplate extends React.Component {
  render() {
    const post = this.props.data.mdx
    const siteTitle = this.props.data.site.siteMetadata.title
    const { previous, next } = this.props.pageContext
    console.log(this.props.pageContext)

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO title={post.frontmatter.title} description={post.excerpt} />
        <Title>
          <h1>{post.frontmatter.title}</h1>
        </Title>
        <Mountains image="/mtns.svg" />
        <Content>
          <MDXProvider components={shortcodes}>
            <MDXRenderer>{post.body}</MDXRenderer>
          </MDXProvider>
          <PostFooter>
            <div>
              <Bio />
            </div>
            <div>
              {post.frontmatter.date}
            </div>
          </PostFooter>
          {/* <Discovery>
            <div>
              {post.frontmatter.categories}
            </div>
            <div>
              {post.frontmatter.tags}
            </div>
          </Discovery> */}
          <PrevNext>
            <ul>
              <li>
                {previous && (
                  <Link to={previous.fields.slug} rel="prev">
                    ← {previous.frontmatter.title}
                  </Link>
                )}
              </li>
              <li>
                {next && (
                  <Link to={next.fields.slug} rel="next">
                    {next.frontmatter.title} →
                  </Link>
                )}
              </li>
            </ul>
          </PrevNext>
        </Content>
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    mdx(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        categories
        tags
      }
      body
    }
  }
`
