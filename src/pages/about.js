import React from 'react'
import styled from 'styled-components'
import Layout from '../components/Layout'
import Content from '../components/Content'

const Title = styled.h1`
  width: 98%;
  margin: 0 auto 1rem;
  padding: 0 0 3rem;
  color: ${props => props.theme.colors.white};
  text-align: center;
`

const Sect = styled.div`
  font-size: 33px;
  padding: 2.2rem 0 1.2rem;
  text-align: center;
`

const Tyler = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin-top: -86px;

  .tyler {
    border: 2px solid ${props => props.theme.colors.gray.whitesmoke};
    border-radius: 500px;
    height: 100px;
    width: 100px;
  }

  h5 {
    margin-top: 1rem;
  }
`

const About = (props) => (
  <Layout location={props.location} title={`∧∨`}>
    <Title>Tyler Andor</Title>
    <Content>
      <Tyler>
        <img className="tyler" src="tyandor-512.jpg" alt="Tyler Andor" />
        <h5>Software Engineer &amp; Philosopher</h5>
      </Tyler>
      <h3>Professional Bio</h3>
      <p>Tyler has been writing software for more than ten years. He was a lead engineer on an award winning learning application. He developed a platform for the nation’s largest student loan servicing company. Additionally, Tyler has contributed to a variety of enterprise software products.</p>
      <p>He began developing software while working as a university professor, as a way to deliver better learning experiences for his online students.</p>
      <Sect>&sect;</Sect>
      <h3>Research Interests</h3>
      <ul>
        <li>Natural Language Processing</li>
        <li>Linguistics &amp; General Semantics</li>
        <li>Ordinary Language Philosophy</li>
        <li>Logic &amp; Mathematical Philosophy</li>
        <li>Philosophy of Science</li>
        <li>Storytelling</li>
        <li>Classical Mythology</li>
        <li>Fantasy &amp; Science Fiction</li>
        <li>Poetry</li>
      </ul>
      <Sect>&sect;</Sect>
      <h3>Design</h3>
      <h4>My Logo</h4>
      <p>is a symbolic form of my last name. It combines two of the symbols used in formal logic for conjunction (and) and disjunction (or).</p>
      <h4>Attributions</h4>
      <p><strong>Typefaces</strong></p>
      <ul>
        <li>Monospace &mdash; <a href="https://github.com/tonsky/FiraCode" target="_blank" rel="nofollow">Fira Code</a></li>
        <li>Serif &mdash; <a href="https://fonts.google.com/specimen/Spectral" target="_blank" rel="nofollow">Spectral</a></li>
        <li>Sans &mdash; <a href="https://fonts.google.com/specimen/Oxygen" target="_blank" rel="nofollow">Oxygen</a></li>
      </ul>
      <p><strong>Graphics</strong></p>
      <ul>
        <li>Background Pattern &mdash; created by <a href="http://luukvanbaars.com/" target="_blank" rel="nofollow">Luuk van Baars</a></li>
      </ul>
    </Content>
  </Layout>
)

export default About
