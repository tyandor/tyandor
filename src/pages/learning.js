import React from 'react'
import styled from 'styled-components'
import Layout from '../components/Layout'
import Content from '../components/Content'

const Title = styled.h1`
  width: 98%;
  margin: 0 auto;
  padding: 0 0 3rem;
  color: ${props => props.theme.colors.white};
  text-align: center;
  text-shadow: 0 1px 4px ${props => props.theme.colors.black};
`

const Learning = (props) => (
  <Layout location={props.location} title={`∧∨`}>
    <Title>Learning</Title>
    <Content>
      <h2>Podcasts</h2>
      <p>Nulla vitae elit libero, a pharetra augue. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
      <iframe src="https://www.listennotes.com/listen/my-podcasts-mLZB-0lHENI/podcasts/embed/" height="525px" width="100%" frameborder="0" scrolling="no"></iframe>
    </Content>
  </Layout>
)

export default Learning

