import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import styled from "styled-components"
import Image from 'gatsby-image'

const BioWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;

  .author {
    display: flex;
    align-items: center;

    img {
      height: 70px;
      width: 70px;
      margin: 0;
      border-radius: 500px;
      border: 1px solid ${props => props.theme.colors.black};
      box-shadow: none;
    }
    span {
      padding: 0 .25rem;

      &.byline {
        margin-left: .25rem;
      }
    }
  }
`

const Bio = () => {
  return (
    <StaticQuery
      query={bioQuery}
      render={data => {
        const { author } = data.site.siteMetadata
        return (
          <BioWrap>
            <div className="author">
              <img
                src="/tyler.andor.png"
                alt={author}
              />
              <span className="byline">
                by <strong>{author}</strong>
              </span>
            </div>
          </BioWrap>
        )
      }}
    />
  )
}

const bioQuery = graphql`
  query BioQuery {
    site {
      siteMetadata {
        author
        social {
          twitter
        }
      }
    }
  }
`

export default Bio
