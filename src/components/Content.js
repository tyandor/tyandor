import styled from "styled-components"

const Content = styled.div`
  width: 100%;
  position: relative;
  margin-top: -2rem;
  padding-top: 2rem;
  color: ${props => props.theme.colors.dark};
  background: #EDF2F3;
  z-index: 2;

  a {
    color: ${props => props.theme.colors.primaryDark};

    &:hover {
      color: ${props => props.theme.colors.primary};
    }
    &:visited {
      color: ${props => props.theme.colors.primaryDark};
    }
  }

  > p {
    margin: 1rem auto;

    &.lead {
      font-size: ${props => props.theme.fontSize.large};
    }

    @media (min-width: ${props => props.theme.breakpoints.phone}) {
      max-width: 700px;
    }
  }
  > h2, > h3, > h4, > h5, > h6 {
    margin-bottom: 1rem;
    padding: 0 1rem;
    color:  ${props => props.theme.colors.dark};

    @media (min-width: ${props => props.theme.breakpoints.phone}) {
      margin: 3rem auto 1rem;
      max-width: 700px;
    }
  }
  > ul, > ol, > blockquote {
    padding-left: 0;
    margin-left: 0;

    @media (min-width: ${props => props.theme.breakpoints.phone}) {
      margin: 2rem auto;
      max-width: 700px;
    }
  }
  ul {
    list-style-type: none;
    margin: 2rem auto;

    li {
      position: relative;
      margin-bottom: 1rem;

      &::before {
        position: absolute;
        content: "\\25CF";
        color: ${props => props.theme.colors.primary};
        font-weight: bold;
        width: 1rem;
        left: -1.5rem;
        height: 100%;
        display: flex;
        align-items: center;
      }
      &::after {
        position: absolute;
        content: "";
        background: ${props => props.theme.colors.primary};
        height: 2px;
        width: 100%;
        left: calc(-100%);
        top: 50%;
        transform: translateY(-50%);
        margin-left: -1.4rem;
      }
    }
  }

  img, audio, video, figure {
    box-shadow: 0px 25px 22px rgba(0,0,0,0.077);

    @media (min-width: ${props => props.theme.breakpoints.phone}) {
      display: flex;
      margin: 3rem auto 4rem;
      max-width: 1000px;
    }
  }

  .text-center {
    text-align: center;
  }
`

export default Content
