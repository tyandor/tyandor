import React from 'react'
import styled from "styled-components"

const Img = styled.div`
  width: 100%;
  position: relative;
  z-index: 2;

  img {
    width: 100%;
    max-width: 100% !important;
    margin-bottom: 0;
    box-shadow: none;
  }
`

const Waves = () => {
  return (
    <Img>
      <img src="/waves.svg" />
    </Img>
  )
}

export default Waves
