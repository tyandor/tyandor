import React from 'react'
import { render } from 'react-dom'
import styled from 'styled-components'
import '../../config/mixins.scss'

const Prjct = styled.article`
  padding: 4.7rem 0;
  margin: 0 auto;
  max-width: 1440px;
  display: flex;
  justify-content: space-between;
  height: 100%;
  width: 100%;
  position: relative;

  @media (max-width: ${props => props.theme.breakpoints.tablet}) {
    flex-direction: column;
    padding: 3rem 0;
  }

  .logo {
    width: 40%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    position: relative;

    img {
      max-width: 84%;
      margin: 1rem auto;
    }

    @media (max-width: ${props => props.theme.breakpoints.tablet}) {
      width: 100%;
    }
  }

  .description {
    width: 60%;
    display: flex;
    filter: drop-shadow(0px 43px 33px rgba(0,0,0,0.43));
    margin: 0;

    .content {
      width: 100%;
      min-height: 500px;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      padding: 9.2rem 5.2rem 7.5rem;
      background: ${props => props.theme.colors.red};
      shape-outside: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
      clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
      color: ${props => props.theme.colors.white};
      margin: 0 2rem;

      a {
        margin-top: 2rem;
        color: ${props => props.theme.colors.white};
        text-decoration: underline;
      }

      @media (max-width: ${props => props.theme.breakpoints.phone}) {
        clip-path: none;
        shape-outside: none;
        padding: 2rem;
        min-height: 300px;
        margin: 0;
      }
    }

    @media (max-width: ${props => props.theme.breakpoints.tablet}) {
      margin: 1rem auto;
      width: 90%;
    }
    @media (max-width: ${props => props.theme.breakpoints.phone}) {
      width: 100%;
      margin: 0;
    }
  }

  &.kvothe {
    flex-direction: row-reverse;

    .description {

      .content {
        background: ${props => props.theme.colors.primaryDark};
        shape-outside: polygon(50% 0%, 100% 38%, 82% 100%, 18% 100%, 0% 38%);
        shape-margin: 20px;
        clip-path: polygon(50% 0%, 100% 38%, 82% 100%, 18% 100%, 0% 38%);
        padding: 12rem 5.7rem 5.7rem;

        @media (max-width: ${props => props.theme.breakpoints.phone}) {
          clip-path: none;
          shape-outside: none;
          padding: 2rem;
          min-height: 300px;
          margin: 0;
        }
      }
    }

    @media (max-width: ${props => props.theme.breakpoints.tablet}) {
      flex-direction: column;
    }
  }
`

const Project = ({ logo, content, link, linkTitle, className }) => {
  return (
    <Prjct className={className}>
      <div className="logo">
        <img src={logo} alt={linkTitle} />
      </div>
      <div className="description">
        <div className="content">
          <p>{content}</p>
          <a href={link} target="_blank">{linkTitle}</a>
        </div>
      </div>
    </Prjct>
  )
}

export default Project
