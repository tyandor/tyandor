import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import styled from "styled-components"

const BookWrap = styled.section`
  padding: 1rem 3rem;
`

const Book = styled.article`
  display: flex;
  align-items: space-between;
  justify-content: center;
  height: 340px;
  max-width: 840px;
  width: 100%;
  margin: 3rem auto;
  background: ${props => props.theme.colors.white};
  border-radius: 9px;
  box-shadow: 0 14px 28px rgba(0,0,0,0.15), 0 10px 10px rgba(0,0,0,0.12);
  color: ${props => props.theme.colors.gray.dark};

  .cover {
    width: 250px;
    height: 100%;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    border-radius: 9px 0 0 9px;
  }

  .details {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-between;
    width: 75%;
    padding: 1rem 2rem;

    .heading {
      .title {
        margin: 0;
        padding: 0;
        font-family: ${props => props.theme.fontFamily.serif};
      }
      .author {
        margin: 0.5rem 0;
        padding: 0;
        font-style: italic;
      }
    }

    .meta {
      width: 100%;
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      font-family: sans-serif;

      span {
        display: block;
        margin-bottom: 0.25rem;
        color: #CCC;
        font-size: 0.5rem;
        font-weight: 700;
        text-transform: uppercase;
      }
      div {
        font-size: 1.25rem;

        &.shelf {
          text-transform: capitalize;

          p {
            margin: 0;
            padding: 0;
          }
        }
      }
    }
  }
`

const Goodreads = () => (
  <StaticQuery
    query={graphql`
      query GoodReadsData {
        allGoodreadsCsv(filter: {Date_Read: {ne: null}}, sort: {fields: Date_Read, order: DESC}) {
          totalCount
          edges {
            node {
              id
              ISBN
              Title
              Author
              Image
              Date_Read
              Bookshelves
              My_Rating
              Original_Publication_Year
            }
          }
        }
      }
    `}
    render={data => (
      <BookWrap>
        <p className="text-center">{data.allGoodreadsCsv.totalCount} Books</p>
        <div>
          {data.allGoodreadsCsv.edges.map(book => (
            <Book key={book.node.id}>
              <div className="cover" style={{backgroundImage: "url(" + book.node.Image + ")"}}></div>
              <div className="details">
                <div className="heading">
                  <h3 className="title">{book.node.Title}</h3>
                  <p className="author">by {book.node.Author}</p>
                </div>
                <div className="meta">
                  <div className="pub">
                    <span>published</span>
                    {book.node.Original_Publication_Year}
                  </div>
                  <div className="rating">
                    <span>my rating</span>
                    {book.node.My_Rating}/5
                  </div>
                  <div className="shelf">
                    <span>shelved under</span>
                    <p>{book.node.Bookshelves}</p>
                  </div>
                </div>
              </div>
            </Book>
          ))}
        </div>
      </BookWrap>
    )}
  />
)

export default Goodreads
