import React from 'react'
import styled from "styled-components"

const Img = styled.div`
  width: calc(100% + 2rem);
  position: relative;
  margin-top: -50px;
  z-index: 1;

  img {
    width: 100%;
    margin: 0 -1rem -1.5rem;
    box-shadow: none;
  }
`

const Mountains = ({image}) => {
  return (
    <Img>
      <img src={image} />
    </Img>
  )
}

export default Mountains
